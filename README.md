# terminated-user-cleanup

Powershell script to delete users in a particular OU after they have been added for X days

Since Active Directory doesn't include a robust way of doing this without digging into auditing, this script will maintain it's own list of users and when they were added to that OU
If a user is removed from the OU, they will subsequently be removed from the list the next time it is run. In other words, if you are set to remove after 30 days, and you added the user 28 days ago but removed them today, they will no longer be deleted in two days. Re-adding them will start them again at 0 days.

Script will create a log file, and the UsersList if they don't already exist. 