# Powershell script to delete users in a particular OU after they have been added for X days
#
# By: Landon Lengyel (landon@almonde.org)
# Version: 2021-08-18


###############
# Variables 
###############

# Path to scripts logs
$ScriptLogs = "./terminated-user-cleanup_log.log"
# Path to csv file (pseudo-database) that will maintain users in the OU, and how long they've been there
$UsersListFile = "./UsersList_DoNotEdit.csv"
# Seperator to use for CSV file, and log
$CsvDelimiter = "|"
# Distinguished Name of the OU you wish to remove disabled users from
$TargetOU = "OU=DisabledAccounts,DC=subdomain,DC=example,DC=com"
# How many days the user object should exist in the OU before being deleted. MUST be a negative number
$DeletionDays = -30


###############
# Functions
###############

## Add-Log is a function to ensure consistency of logs, as well as allowing future additions to the log without updating the underlying code of the script
## Depends on $ScriptLogs global variable
## Does not return anything
function Add-Log {
    param (
        # Log Type allows for easy filtering. (Ex. Info, Error, FatalError, Change, etc)
        [Parameter(Mandatory=$True)]
        [string]$LogType,

        # Details for the specifics on what is being logged
        [Parameter(Mandatory=$True)]
        [string]$Details
    )

    # Date Time is in ISO 8601 format
    $DateTime = Get-Date -Format "yyyy-MM-ddTHH:mm:ss"

    # When editing, also edit the "Creating log file" code if needed
    # Columns: Date Time, Log Type, Details
    try { Add-Content -Path $ScriptLogs -Value ($DateTime + $CsvDelimiter + $LogType + $CsvDelimiter + $Details) }
    catch {
        Write-Host -ForegroundColor Red "Error writing to the log file. Exiting..."
        Write-Error "Error creating and/or writing to log file. Exiting..."
        # Exiting to ensure script doesn't run without logging
        exit
    }

}


################
# Main
################


## Create files (if they don't exist already) and import data. Failure in this section is fatal will exit the script.

# Create log file (if necessary)
if (Test-Path $ScriptLogs) {
    Write-Host "Log file already exists. Continuing to add data to end of the file"
}
else {
    try { 
        New-Item $ScriptLogs
        Add-Content -Path $ScriptLogs -Value ("Timestamp" + $CsvDelimiter + "Log Type" + $CsvDelimiter +  "Details")
    }
    catch {
        # Writing in multiple streams in an attempt to notify an admin since the log file is unavailable
        Write-Host "Error creating and/or writing to log file. Exiting..."
        Write-Error "Error creating and/or writing to log file. Exiting..."
        # Exiting to ensure script doesn't run without logging
        exit
    }
}

# Log starting now that log file exists
Add-Log -LogType "Info" -Details "Script starting"

# Create users list (if necessary)
if (Test-Path $UsersListFile) {
    Write-Host "Users file already exists. Continuing to add data to end of the file"
}
else {
    try { 
        New-Item $UsersListFile
        Add-Content -Path $UsersListFile -Value ("SamAccountName" + $CsvDelimiter + "Timestamp" + $CsvDelimiter +  "ObjectGUID")
    }
    catch {
        Add-Log -LogType "FatalError" -Details "Could not create users list file. Exiting. Path: $UsersListFile"
        exit
    }
}


# Import the UsersList for modification
# UsersListFile should follow format: Username | Date added | ObjectGUID
# This is also used after editing the file
try { $UsersList = Import-CSV -Delimiter $CsvDelimiter -Path $UsersListFile }
catch {
    Add-Log -LogType "FatalError" -Details "Could not import users list file. Exiting. Path: $UsersListFile"
    exit
}


# Module to view/modify AD users
try { import-module activedirectory }
catch {
    Add-Log -LogType "FatalError" -Details "Could not import-module activedirectory. Exiting."
    exit
}

# Get a list of all users in the TargetOU
try { $CurrentADUsers = Get-ADUser -SearchBase $TargetOU -Filter * -Properties Name, SamAccountName, ObjectGUID }
catch {
    Add-Log -LogType "FatalError" -Details "Could not get users from Active Directory. Exiting. OU: $TargetOU"
    exit
}

# Add log notifying how many users are in the OU
Add-Log -LogType "Info" -Details ("Number of users in TargetOU: " + $CurrentADUsers.Count)

## Add new user objects to the UsersList
# Loop through AD users comparing ObjectGUID to users in file 
foreach ($CurrentADUser in $CurrentADUsers) {
    # Start assuming no match, and change to true if found
    $FileMatch = $False

    # Comparing third column of file (ObjectGUID)
    foreach ($UsersListUser in $UsersList) {
        if ($UsersListUser.ObjectGUID -eq $CurrentADUser.ObjectGUID) {
            $FileMatch = $True
            break
        }
    }

    # If it has looped through all users in the file and found no match, add them to the file with current date
    if ($FileMatch -eq $False) {
        Write-Host -ForegroundColor Yellow ("New user in TargetOU: " + $CurrentADUser.SamAccountName)
        $DateTime = Get-Date -Format "yyyy-MM-ddTHH:mm:ss"
        Add-Content -Path $UsersListFile -Value ( $CurrentADUser.SamAccountName + $CsvDelimiter + $DateTime + $CsvDelimiter + $CurrentADUser.ObjectGUID )
    }
}


## Remove old user objects from the UsersList
# This should basically be reversed logic from the above section
foreach ($UsersListUser in $UsersList) {
    # Start assuming no match, and change to true if found
    $FileMatch = $False

    # Comparing third column of file (ObjectGUID)
    #foreach ($CurrentUser in $CurrentADUsers) {
    for ($CurrentADUsersNumber = 0; $CurrentADUsersNumber -lt $CurrentADUsers.Count; $CurrentADUsersNumber++) {
        if ($UsersListUser.ObjectGUID -eq $CurrentADUsers[$CurrentADUsersNumber].ObjectGUID) {
            $FileMatch = $True
            break
        }
    }

    # If it has looped through all users in AD and found no match, remove them from the file
    if ($FileMatch -eq $False) {
        # File needs to be reloaded because otherwise it will save a PSObject
        Write-Host -ForegroundColor Yellow ("User no longer in TargetOU, removing: " + $UsersListUser.SamAccountName)
        (Get-Content $UsersListFile) -notmatch $UsersListUser.ObjectGUID | Set-Content $UsersListFile
    }
}

# Re-Import the users list after removing people from it to ensure it's up to date
try { $UsersList = Import-CSV -Delimiter $CsvDelimiter -Path $UsersListFile }
catch {
    Add-Log -LogType "FatalError" -Details "Could not import users list file. Exiting. Path: $UsersListFile"
    exit
}


## Loop through each user in the file, and delete if they are older than the $DeletionDays
foreach ($UsersListUser in $UsersList) {
    # If the user was added to the UserList longer than $DeletionDays ago
    if ([datetime]$UsersListUser.Timestamp -le (Get-Date).AddDays($DeletionDays)) {
        $OldUser = Get-ADUser -id $UsersListUser.ObjectGUID -Properties Name, SamAccountName, ObjectGUID

        # Perform final verification that this user is in the $TargetOU
        if (($OldUser.DistinguishedName).EndsWith($TargetOU)) {
            Write-Host -ForegroundColor Green ("User in OU exceeded defined time of " + $DeletionDays + "days. Deleting from AD: " + $Olduser.SamAccountName + "--" + $OldUser.ObjectGUID)
            Add-Log -LogType "Change" -Details ("User in OU exceeded defined time of " + $DeletionDays + "days. Deleting from AD: " + $Olduser.SamAccountName + "--" + $OldUser.ObjectGUID)
            # Delete the user from Active Directory
            # This one prompts for confirmation, best for testing
            #Remove-ADUser -Identity $OldUser.ObjectGUID
            # This one does not prompt for confirmation. Best for production
            Remove-ADUser -Identity $OldUser.ObjectGUID -Confirm:$False
        }
        else {
            Write-Host -ForegroundColor Red ("User may have been moved out of OU while script was running. Doing nothing:" + $Olduser.SamAccountName + "--" + $OldUser.ObjectGUID)
            Add-Log -LogType "Error" -Details ("User may have been moved out of OU while script was running. Doing nothing:" + $Olduser.SamAccountName + "--" + $OldUser.ObjectGUID)
        }
    }
}

Add-Log -LogType "Info" -Details "Script complete"